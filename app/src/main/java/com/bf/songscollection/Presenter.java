package com.bf.songscollection;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class Presenter {

    private MainView view;
    private SongsApi songsApi;
    private DatabaseRealm database;
    private Subscription subscription;

    public Presenter(SongsApi songsApi, DatabaseRealm database) {
        this.songsApi = songsApi;
        this.database = database;
    }

    public void bindView(MainView mainView) {
        view = mainView;
        loadSongsFromDatabase();
        subscription = songsApi.songs().getSongsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Action1<List<Song>>() {
                    @Override public void call(List<Song> songs) {
                        view.onSongsLoaded(songs);
                        database.saveObjects(songs);
                    }
                }, new Action1<Throwable>() {
                    @Override public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        view.onError(throwable.getMessage());
                    }
                });

    }

    private void loadSongsFromDatabase() {
        view.onSongsLoaded(database.getSongs());
    }

    public void detachView() {
        if (subscription != null)
            subscription.unsubscribe();
    }

    public interface MainView {
        void onSongsLoaded(List<Song> songs);
        void onError(String message);
    }
}
