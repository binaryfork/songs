package com.bf.songscollection;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<SongVH> {

    private final LayoutInflater inflater;
    private List<Song> data;

    public SongAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @Override public SongVH onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SongVH(inflater.inflate(R.layout.song_item, parent, false));
    }

    @Override public void onBindViewHolder(SongVH holder, int position) {
        holder.bind(data.get(position));
    }

    @Override public int getItemCount() {
        if (data == null)
            return 0;
        else
            return data.size();
    }

    public void setData(List<Song> songList) {
        ArrayList<Song> oldData = new ArrayList<>();
        if (getItemCount() > 0)
            oldData.addAll(data);
        data = new ArrayList<>();
        data.addAll(songList);

        if (getItemCount() > 0) {
            for (int i = 0; i < oldData.size(); i++) {
                Song oldSong = oldData.get(i);
                boolean delete = true;
                for (Song newSong : songList) {
                    if (newSong.getId() == oldSong.getId()) {
                        delete = false;
                        if (newSong.getVersion() > oldSong.getVersion())
                            notifyItemChanged(i);
                    }
                }
                if (delete)
                    notifyItemRemoved(i);
            }
            if (songList.size() > oldData.size()) {
                notifyItemRangeInserted(oldData.size(), songList.size() - oldData.size());
            }
        }
    }
}
