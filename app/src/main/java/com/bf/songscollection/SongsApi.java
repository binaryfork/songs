package com.bf.songscollection;

import retrofit2.Retrofit;

public class SongsApi {

    private Retrofit retrofit;

    public SongsApi(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    public SongsService songs() {
        return retrofit.create(SongsService.class);
    }
}
