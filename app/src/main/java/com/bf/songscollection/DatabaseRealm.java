package com.bf.songscollection;

import android.content.Context;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;

public class DatabaseRealm {

    public Realm realm;

    public DatabaseRealm(Context context) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context)
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(realmConfiguration);
    }

    public <T extends RealmObject> void saveObject(T object) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(object);
        realm.commitTransaction();
    }

    public <T extends RealmObject> List<T> saveObjects(List<T> contents) {
        realm.beginTransaction();
        List<T> list = realm.copyToRealmOrUpdate(contents);
        realm.commitTransaction();
        return list;
    }

    public List<Song> getSongs() {
        return realm.where(Song.class).findAll();
    }
}
