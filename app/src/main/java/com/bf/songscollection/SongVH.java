package com.bf.songscollection;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SongVH extends RecyclerView.ViewHolder {

    @BindView(R.id.author) TextView authorText;
    @BindView(R.id.song) TextView songText;

    public SongVH(View view) {
        super(view);
        ButterKnife.bind(this, view);
        setIsRecyclable(true);
    }

    public void bind(Song song) {
        authorText.setText(song.getAuthor());
        songText.setText(song.getLabel());
    }
}
