package com.bf.songscollection;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface SongsService {
    @GET("songs") Observable<List<Song>> getSongsList();
}
