package com.bf.songscollection;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.Component;
import jp.wasabeef.recyclerview.animators.ScaleInAnimator;

public class MainActivity extends AppCompatActivity implements Presenter.MainView {

    @Inject Presenter presenter;

    @BindView(R.id.recycler) RecyclerView recyclerView;
    @BindView(R.id.parent_layout) SwipeRefreshLayout refreshLayout;
    private SongAdapter songAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((BaseApp) getApplication()).getAppComponent().inject(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new GridDividerDecoration(getApplicationContext()));
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new ScaleInAnimator(new DecelerateInterpolator(1f)));

        songAdapter = new SongAdapter(this);
        recyclerView.setAdapter(songAdapter);

        presenter.bindView(this);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override public void onRefresh() {
                presenter.bindView(MainActivity.this);
            }
        });
    }

    @Override public void onSongsLoaded(List<Song> songs) {
        refreshLayout.setRefreshing(false);
        songAdapter.setData(songs);
    }

    @Override public void onError(String message) {
        refreshLayout.setRefreshing(false);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Singleton
    @Component(modules = {NetModule.class}) interface MainActivityComponent {
        void inject(MainActivity mainActivity);
    }
}
