package com.bf.songscollection;

import android.app.Application;

public class BaseApp extends Application {

    private MainActivity.MainActivityComponent appComponent;

    @Override public void onCreate() {
        super.onCreate();
        appComponent = DaggerMainActivity_MainActivityComponent.builder()
                .netModule(new NetModule(this))
                .build();
    }

    public MainActivity.MainActivityComponent getAppComponent() {
        return appComponent;
    }
}
