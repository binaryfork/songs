package com.bf.songscollection;

import android.content.Context;

import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    private Context context;

    public NetModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Context providesApplication() {
        return context;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
            okClientBuilder.addInterceptor(httpLoggingInterceptor);
        }
        return okClientBuilder.build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("http://tomcat.kilograpp.com/songs/api/")
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    SongsApi provideSolityApi(Retrofit retrofit) {
        return new SongsApi(retrofit);
    }

    @Provides
    @Singleton
    DatabaseRealm provideDatabase() {
        return new DatabaseRealm(context);
    }

    @Provides
    @Singleton
    Presenter providePresenter(SongsApi songsApi, DatabaseRealm databaseRealm) {
        return new Presenter(songsApi, databaseRealm);
    }

}
